#!/usr/bin/python3

import pprint # useful for debugging
import os
import sys,getpass
import json,urllib.request
from requests.utils import parse_header_links

private_token = ''


# Extracts the next page's link from an
# http.client.HTTPResponse object, such as
# what's returned by urllib.request.urlopen()
# Returns False if there is no link for the next page.
# Otherwise, returns the link as a string.
def get_next_page_link(http_response):
    link_header = http_response.getheader('Link')
    if not link_header: return False
    parsed_link_header = parse_header_links(link_header)
    for link in parsed_link_header:
        if 'rel' in link and link['rel'] == 'next' and 'url' in link:
            return link['url']
    return False

# request makes a request to https://git.uwaterloo.ca/api/v4/
# and returns the JSON data as a Python object. If the request has
# multiple pages, this function will attempt to automatically retrieve
# all the records.
# Input: query: Part of URL after the URL above
#        post_hash: A dictionary of data to send in a POST request
#        query_headers: Any headers you want to send as part of the request
#        quit_on_error: If True, will quit program on error. If False, will
#                       try (max_tries-1) more times before returning False
#        max_tries: Number of attempts to make per request to get data from git.uwaterloo.ca
#        print_errors: If True, will print errors to stdout.
# Returns: A python object
def request(query, post_hash={}, query_headers={}, http_method=None, quit_on_error=False, max_tries=3, print_errors=True):
    # Add private token if it's not in header yet
    if 'PRIVATE-TOKEN' not in query_headers:
        query_headers['PRIVATE-TOKEN'] = private_token

    # Encode the POST data
    post_data = urllib.parse.urlencode(post_hash).encode('ascii') if post_hash else None

    # Gitlab API does not always return all the data at once. Sometimes the data
    # is spread across multiple pages. See Gitlab API doc on pagination for detalis.
    current_page_link = query if query.startswith('http') else ("https://git.uwaterloo.ca/api/v4/"+query)
    accumulated_data = []

    # Keep looping until there are no more pages to process. Most requests will only have 1 page
    while True:
        # Get data for a page
        request_attempt = 1; success = False
        # Try to get data from Gitlab. We make several attempts in case there
        # are random, intermittent problems (ex. network issues)
        while request_attempt <= max_tries and not success:
            req = urllib.request.Request(url=current_page_link, data=post_data, headers=query_headers, method=http_method)
            try:
                with urllib.request.urlopen(req) as http_response:
                    next_page_link = get_next_page_link(http_response)
                    json_string = http_response.read().decode('utf-8')
                    try:
                        python_object = json.loads(json_string)
                    except Exception as e:
                        if print_errors:
                            print(json_string)
                            print("Error occurred trying to interpret above data as JSON.")
                            print("Error message: %s" % str(e))
                        if quit_on_error:
                            sys.exit(1)
                        else:
                            return False
                    success = True
                    break
            except Exception as e:
                if print_errors:
                    print("Error occurred trying to access %s" % current_page_link)
                    print("Error %s message: %s" % (type(e).__name__, str(e)))
                if quit_on_error:
                    sys.exit(1)
                else:
                    request_attempt += 1
                    if print_errors and request_attempt <= max_tries:
                        print("Retrying... (attempt number %d)" % request_attempt)
                    continue

        # Finished the while loop over attempts

        if not success:
            if print_errors: print("Request failed after %d attempts" % max_tries)
            return False

        # Add the data from this page to accumulated data, and continue
        # to next page if needed
        if (not next_page_link) and (not accumulated_data):
            # No next page, and no data from previous pages
            return python_object
        else:
            # Add this page's data to our accumulated data
            if isinstance(python_object, list):
                accumulated_data.extend(python_object)
            else:
                accumulated_data.append(python_object)
            # Need to check next page?
            if next_page_link:
                current_page_link = next_page_link
                continue # to next page
            else:
                return accumulated_data
    # We shouldn't reach here because the "while True" loop will exit
    # via return, but we return here just in case.
    return accumulated_data

# Read private token from token_file. Mutates the global private_token
# above and returns it too.
def set_private_token(token_file):
    global private_token
    if token_file == "/dev/stdin":
        print("You can create a Gitlab private token at https://git.uwaterloo.ca/profile/personal_access_tokens")
        private_token = getpass.getpass("Please enter your Gitlab private token:")
        return private_token
    else:
        try:
            token_file_handle = open(token_file, 'r')
            private_token = token_file_handle.readline().strip()
            token_file_handle.close()
            return private_token
        except Exception as e:
            print("Error occurred trying to read private token from file %s" % token_file)
            print("Error message: %s" % str(e))
            sys.exit(1)

# Returns the group id (an integer) of group_name. If group_name could
# not be found, prints the groups available and exit.
def get_group_id(group_name):
    groups_data = request('groups?per_page=1000000')
    for group in groups_data:
        if group['name'].lower() == group_name.lower():
            return group['id']
    # could not find a group with the given name
    print("Could not find group %s." % group_name)
    print("The groups that are available are:")
    name_width = 20
    print(os.linesep)
    print("\t%s   Description" % ("Name".ljust(name_width)))
    print("\t%s   ---------------" % ("-" * name_width))
    for group in groups_data:
        print("\t%s   %s" % (group['name'].ljust(name_width), group['description']))
    print(os.linesep)
    sys.exit(1)
