#!/usr/bin/python3

import time
import argparse,getpass,re
import sys,subprocess,os
import json,urllib.request
import gitlabhelpers
import ldaphelpers
import pprint

# Parse command-line arguments.
parser = argparse.ArgumentParser(description="This script is used to create student repositories.")
parser.add_argument('group_name', help="The name of the Gitlab group to create projects in.")
parser.add_argument('--token-file', default="/dev/stdin",
                    help="Path to file containing your Gitlab private token. Default is to read from standard input.")
parser.add_argument('--cookie-file', default="/dev/stdin", help="This option is ignored and no longer needed to add students.")
parser.add_argument('--add-students', action='store_true',
                    help="By default, students will not be added to their repos. Set this option to add them, which will email them too.")
parser.add_argument('--assns', default="A0,A1,A2,A3,A4",
                    help="Comma separated list of assignment folders to create when setting up the repo. Default is: A0,A1,A2,A3,A4")
students_arg_group = parser.add_mutually_exclusive_group()
students_arg_group.add_argument('--odyssey', action='store_true', help="Use students from `odyssey classlist` tool.")
students_arg_group.add_argument('--classlist', nargs=1, help="Path to your course's .classlist file on the student.cs Linux servers.")
students_arg_group.add_argument('--students', help="A comma separated list of student Quest IDs. Create repositories for these students only.")
args = parser.parse_args()

# save command line argument inputs in variables
group_name = args.group_name
token_file = args.token_file
add_students = args.add_students

# Get list of assignment folders to create when setting up the repo
assns = list(filter(lambda s: s, map(lambda s: s.strip(), args.assns.split(','))))
if not assns:
    print("Must specify at least one assignment folder to create. Quitting...")
    sys.exit(1)

# Read private token from keyboard or from file
gitlabhelpers.set_private_token(token_file)

# Students we will create repositories for
students = []
if args.students:
    students = list(map(lambda s:s.strip(), args.students.split(',')))
    students = list(map(lambda s:s[:8],students))
elif args.odyssey:
    odyssey_process = subprocess.run(["odyssey", "classlist", "--no-header", "--columns", "userid"], stdout=subprocess.PIPE)
    if odyssey_process.returncode == 0:
        for student_userid in odyssey_process.stdout.decode("utf-8").splitlines():
            students.append(student_userid.strip()[0:8])
    else:
        print("odyssey classlist tool exited with non-zero code %d" % odyssey_process.returncode)
        sys.exit(1)
elif args.classlist:
    classlist_regex = re.compile('^[0-9]{8}:([a-z0-9]+):')
    classlist_file = args.classlist[0]
    for line in open(classlist_file, 'r'):
        match = classlist_regex.match(line)
        if match != None:
            userid = match.group(1)
            userid = userid[0:8]
            students.append(userid)

students = list(filter(lambda s: s and not s.isspace(), students))
if not students:
    print("No students to process. Quitting...")
    sys.exit(0)

# Create a hash mapping student usernames to the id of their project/repo
# This should be empty. If not, it means some projects have already
# been created.
group_id = gitlabhelpers.get_group_id(group_name)
projects_data = gitlabhelpers.request("groups/%d/projects?per_page=100000" % group_id)
project_ids = {}
for project in projects_data:
    username = project['ssh_url_to_repo'].rsplit('/',1)[-1][:-4]
    project_ids[username] = project['id']

# Begin processing students
print("Processing %d total students." % len(students))
first_loop = True
for student in students:

    # Put in a delay between loop iterations so that git.uwaterloo.ca isn't hammered
    if not first_loop: time.sleep(5)
    first_loop = False

    print(os.linesep)
    print('-' * 60)
    print("> Processing %s" % student)
    
    # Create project/repo for students who do not have one yet.
    if student not in project_ids:
        # Student doesn't have a project/repo yet. Create it
        print("> %s doesn't have a project/repo yet. Creating it now." % student)
        new_project = gitlabhelpers.request('projects', post_hash={'name':student, 'namespace_id':group_id, 'visibility':'private'})
        project_ids[student] = new_project['id']
        print("> Created new project with id %d" % new_project['id'])
    else:
        print("> %s already has a project (id %d). Not creating it again." % (student, project_ids[student]))

    # Create master branch if it doesn't exist yet
    existing_branches = gitlabhelpers.request('projects/%d/repository/branches' % project_ids[student])
    master_branch_exists = False
    for branch in existing_branches:
        if branch['name'] == 'master':
            master_branch_exists = True
    if not master_branch_exists:
        print("> master branch doesn't exist for %s. Creating it." % student)
        time.sleep(3)
        print("> Creating assignment folders:", ', '.join(assns))
        for assn in assns:
            print("> Doing work for assignment %s" % assn)
            gitlabhelpers.request('projects/%d/repository/files/%s' % (project_ids[student], urllib.parse.quote_plus("%s/.gitignore"%assn)), \
                           post_hash={'branch':"master", 'content':"*.class\n", 'commit_message':("Creating %s folder" % assn)})

        # Wait for master branch to become protected. Gitlab seems to have a delay on protecting the
        # master branch when it's created.
        while True:
            master_branch_info = gitlabhelpers.request('/projects/%d/repository/branches/master' % project_ids[student], quit_on_error=False)
            if master_branch_info and master_branch_info['protected']:
                print("> Newly created master branch has become protected.")
                break
            print("> Waiting for Gitlab to make newly created master branch protected.")
            time.sleep(1) # Don't spam Gitlab website
    else:
        print("> master branch already exists for %s. Not creating it." % student)

    # Turn off master branch protection (on by default). At this point
    # in the code, we have created master branch if it doesn't exist.
    # So master branch should exist. Also, if master is already unprotected,
    # then this operation does nothing (it's idempotent).
    print("> Unprotecting master branch.")
    gitlabhelpers.request('/projects/%d/repository/branches/master/unprotect' % project_ids[student], http_method='PUT')
        
    # The repo is now set up with an unprotected master branch.
    # Do email invitation if user wants to do that.
    if add_students:
        print("> Adding student to project/repository.")

        student_email = ldaphelpers.get_student_email(student)
        access_level = 30 # 30 is Developer, see https://docs.gitlab.com/ee/api/access_requests.html
        invite_response = gitlabhelpers.request('/projects/%d/invitations' % project_ids[student], \
            post_hash={'email':student_email, 'access_level':access_level})
        if invite_response and ('status' in invite_response) and (invite_response['status'] == 'success'):
            print("> Successfully invited %s" % student_email)
        else:
            print("> Could not add invitation email: %s" % student_email)
            if invite_response and 'message' in invite_response:
                print("> Gitlab response:")
                pprint.pprint(invite_response['message'])

    print("> Done processing %s." % student)
